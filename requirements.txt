beautifulsoup4==4.8.2
boto3==1.12.31
botocore==1.15.31
CacheControl==0.12.6
cachetools==4.1.0
certifi==2019.11.28
chardet==3.0.4
click==7.1.1
Django==2.1.5
docutils==0.15.2
firebase-admin==4.0.1
gcloud==0.18.3
google-api-core==1.17.0
google-api-python-client==1.8.1
google-auth==1.14.0
google-auth-httplib2==0.0.3
google-cloud-core==1.3.0
google-cloud-firestore==1.6.2
google-cloud-storage==1.27.0
google-resumable-media==0.5.0
googleapis-common-protos==1.51.0
grpcio==1.28.1
gTTS==2.1.1
gTTS-token==1.1.3
httplib2==0.17.2
idna==2.9
jmespath==0.9.5
jws==0.1.3
msgpack==1.0.0
oauth2client==4.1.2
protobuf==3.11.3
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycryptodome==3.9.7
Pyrebase4==4.3.0
python-dateutil==2.8.1
python-jwt==2.0.1
pytz==2019.3
requests==2.23.0
requests-toolbelt==0.9.1
rsa==4.0
s3transfer==0.3.3
setuptools>=40.3.0
six==1.14.0
soupsieve==2.0
uritemplate==3.0.1
urllib3==1.25.8
whitenoise==5.0.1
youtube-dl==2020.3.24
