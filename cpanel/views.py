from django.shortcuts import render
import pyrebase
from django.contrib import auth
config = {

    'apiKey': "YOUR-API-KEY",
    'authDomain': "covid19-indian.web.app",
    'databaseURL': "https://covid19-indian.firebaseio.com",
    'projectId': "covid19-indian",
    'storageBucket': "covid19-indian.appspot.com",
    'messagingSenderId': "307309XXXXXX",
    'serviceAccount': 'covid19-indian.json'
  }

firebase = pyrebase.initialize_app(config)

authe = firebase.auth()
database=firebase.database()
# Begins

# import firebase_admin
# from firebase_admin import credentials

# cred = credentials.Certificate("covid19-indian.json")
# firebase_admin.initialize_app(cred)

# Ends
# def signIn(request):

#     return render(request, "signIn.html")

# def postsign(request):
#     email=request.POST.get('email')
#     passw = request.POST.get("pass")
#     try:
#         user = authe.sign_in_with_email_and_password(email,passw)
#     except:
#         message="invalid credentials"
#         return render(request,"signIn.html",{"messg":message})
#     print(user['idToken'])
#     session_id=user['idToken']
#     request.session['uid']=str(session_id)
#     return render(request, "welcome.html",{"e":email})
# def logout(request):
#     auth.logout(request)
#     return render(request,'signIn.html')


# def signUp(request):

#     return render(request,"signup.html")
# def postsignup(request):

#     name=request.POST.get('name')
#     email=request.POST.get('email')
#     passw=request.POST.get('pass')
#     try:
#         user=authe.create_user_with_email_and_password(email,passw)
#     except:
#         message="Unable to create account try again"
#         return render(request,"signup.html",{"messg":message})
#         uid = user['localId']

#     data={"name":name,"status":"1"}

#     database.child("users").child(uid).child("details").set(data)
#     return render(request,"signIn.html")

def create(request):

    return render(request,'create.html')


def post_create(request):

    import time
    from datetime import datetime, timezone
    import pytz

    tz= pytz.timezone('Asia/Kolkata')
    time_now= datetime.now(timezone.utc).astimezone(tz)
    millis = int(time.mktime(time_now.timetuple()))
    name = request.POST.get('name')
    email = request.POST.get('email')
    password = request.POST.get('password')

    # idtoken= request.session['uid']
    data = {
        'name': name,
        'email': email,
        'password': password,
        'timestamp': str(datetime.now())
    }
    database.child('data').child(millis).set(data)
    return render(request,'thankyou.html', {'e': data['name'], 't': data['timestamp']})
